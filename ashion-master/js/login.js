$(document).ready(function() {
    //----Phần này làm sau khi đã làm trang info.js
    var User = {};
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const token = getCookie("token");
    
    if(token) {
        responseHandler(token);        
    }
    var formData = {
        username : "",
        password : "",
        roles:  [
            {
                "id": 2,
                "deleted": false,
                "roleName": "Khách",
                "roleKey": "ROLE_CUSTOMER",
                "permissions": [
                    {
                        "id": 1,
                        "deleted": false,
                        "permissionName": "tạo user",
                        "permissionKey": "USER_CREATE"
                    }
                ]
            }
        ]
    }
    var formDataCustomer = {
        firstname : "",
        lastname : "",
        phoneNumber : "",
        address : ""
    }
    //----Phần này làm sau khi đã làm trang info.js

      // Khi Click nut đăng xuất
      $("#btn-dang-xuat").on("click", function(){
        redirectToLogin();
        displayUserLogOut();
      })


    // Khi Click nut register
    $("#btn-Register").on("click", function(){
        formData.username = $("#inp-register_username").val().trim();
        formData.password = $("#inp-register_password").val().trim();
        formDataCustomer.firstname = $("#inp-firstname").val().trim();
        formDataCustomer.lastname = $("#inp-lastname").val().trim();
        formDataCustomer.address = $("#inp-address").val().trim();
        formDataCustomer.phoneNumber = $("#inp-phone").val().trim();
       
        if(validateForm(formData) && validateFormCustomer(formDataCustomer)) {
            if(formData.password === $("#inp-register_password2").val().trim()){
                callAPIRegister(formData , formDataCustomer);
            }else{
                alert("2 mật khẩu không giống nhau");
                
            }
        }
        })

    //Sự kiện bấm nút login
    $("#btn-login").on("click", function() {    
        formData.username = $("#inp-username").val().trim();
        formData.password = $("#inp-password").val().trim();

        if(validateForm(formData)) {
            signinForm(formData);
        }
    });


    function signinForm(data) {
        "use strict"
        $.ajax({
            url: "http://localhost:8080/login",
            type: "POST",
            async:false,
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function(res){
                responseHandler(res);
            },
            error: function(err){
            alert("Tài Khoản Hoặc Mật Khẩu Không Chính Xác")
            }
            })
    }
    function callAPIRegister(Data , DataCustomer){
        $.ajax({
            url: "http://localhost:8080/register",
            type: "POST",
            async:false,
            data: JSON.stringify(Data),
            contentType: 'application/json',
            success: function(res){
                console.log(res);
                callAPICreateCustomer(DataCustomer ,res)
                // alert("bạn đã tạo tài khoản thành công");
                // $("#myModal-register").modal("hide");
                // $("#myModal-SignIn").modal("show");
            },
            error: function(err){
            alert("Tài Khoản Đã Tồn Tại")
            }
            })
    }
    function callAPICreateCustomer(DataCustomer , res){
        $.ajax({
            url: "http://localhost:8080/customers/user/"+ res.id,
            type: "POST",
            async:false,
            data: JSON.stringify(DataCustomer),
            contentType: 'application/json',
            success: function(res){
                console.log(res);
            },
            error: function(err){
            alert("Đã Tạo Tài Khoản Thành Công, Nhưng Thông Tin Khách Hàng Bị Lỗi Cần Cập Nhập Sau");
            }
            })
    }

  
    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        //Lưu token vào cookie trong 1 ngày
        setCookie("token", data, 1);
          //Khai báo xác thực ở headers
            var headers = {
                Authorization: "Token " + data
            };

        $.ajax({
            url: "http://localhost:8080/infoUser",
            method: "GET",
            headers: headers,
            dataType: "json",
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(responseObject) {
                if(responseObject[1] == "ROLE_ADMIN" || responseObject[1] == "ROLE_MANAGER"){
                    window.location.href = "http://localhost/nuochoa/ShopPlusFrontend/customers.html"
                }else{
                    User = responseObject;
                    window.location.href = "http://localhost/nuochoa/customersShopPlus/ashion-master/ashion-master/"
                }
            },
            error: function(xhr) {
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin();
            }
        });
          // window.location.href = "http://localhost:8080/user/me";
    }
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        // window.location.href = "login.html";
        $("#inp-email").val("");
        $("#inp-password").val("");
        $("#remember").prop("checked" , false);
        $("#show-customer").hide();
        $("#btn-my-bds").hide();
    }


    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Validate dữ liệu từ form
    function validateForm(data) {
        if(data.username == "") {
            alert("username không phù hợp");
            return false;
        };

        if(data.password == "") {
            alert("Password không phù hợp");
            return false;
        }

        return true;
    }
    //Validate dữ liệu từ form
    function validateFormCustomer(data) {
        if(data.contactName == "") {
            alert("Tên Khách Hàng không phù hợp");
            return false;
        };

        if(data.phoneNumber == "") {
            alert("Số Điện Thoại không phù hợp");
            return false;
        }
        if(data.email == "") {
            alert("Địa Chỉ Email không phù hợp");
            return false;
        }
        if(!validateEmail(data.email)){
            alert("Địa Chỉ Email không phù hợp");
            return false;
        }

        return true;
    }

        // hàm kiểm tra email đúng chuẩn 
    function validateEmail(paramEmail){
            "use strict"
            const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return vREG.test(String(paramEmail).toLowerCase());
        };


    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function callApiGetCustomer(Username){
        $.ajax({
            url: "http://localhost:8080/customers/username/" + Username,
            type: "GET",
            dataType: "json",
            async:false,
            success: function (responseObject) {
              displayCustomer(responseObject);
            },
            error: function (error) {
              console.assert(error.responseText);
            },
          });
    }
    function callApiUpdate(data){
        console.log(data);
        $.ajax({
            url: "http://localhost:8080/customers/" + $("#p-id").html(),
            type: "PUT",
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: function(res){
                alert("cập nhập thông tin thành công")
                displayCustomer(res);
            },
            error: function(err){
              console.log(err.response);
              alert("Cập Nhập Thông Tin Không Thành Công")
            }
            })
    }

    // xử lý khi lấy được thông tin user
    function displayUser(){
        $("#nav-dangnhap").hide();
        $("#nav-dang-xuat").show();
        $("#Login-name").text(User[0]);
        $("#post-realestate-link").attr("class","nav-link btn btn-primary rounded")
    }
    //xử lý khi lấy được thông tin user
    function displayCustomer(data){
        $("#p-contactName").val(data.contactName);
        $("#p-contactTitle").val(data.contactTitle);
        $("#p-address").val(data.address);
        $("#p-phoneNumber").val(data.phoneNumber);
        $("#p-email").val(data.email);
        $("#p-createDate").val(data.createDate);
        $("#p-id").html(data.id);

    }
    // xử lý khi Đăng Xuất
    function displayUserLogOut(Data){
        $("#nav-dangnhap").show();
        $("#nav-dang-xuat").hide();
        $("#post-realestate-link").attr("class","nav-link");
        $("#p-contactName").val("");
        $("#p-contactTitle").val("");
        $("#p-address").val("");
        $("#p-phoneNumber").val("");
        $("#p-email").val("");
        $("#p-createDate").val("");
        $("#p-id").html("");
    }



    // Ẩn Hiện Mật Khẩu
    $(document).on("click", ".eyes .fa-eye" , function(e){
        $(this).hide();
        $(this).closest(".eyes").children(".fa-eye-slash").show();
        $(this).closest(".eyes").children("input").attr("type", "password");
    })
    // Ẩn Hiện Mật Khẩu
    $(document).on("click", ".eyes .fa-eye-slash" , function(e){
        $(this).hide();
        $(this).closest(".eyes").children(".fa-eye").show();
        $(this).closest(".eyes").children("input").attr("type", "text");
    })
    
});