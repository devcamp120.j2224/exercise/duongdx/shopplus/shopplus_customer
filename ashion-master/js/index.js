$(document).ready(function(){
    "use strict";
    //Vùng 1: định nghĩa global variable (biến toàn cục)
    const LocalURL = `http://localhost:8080/` ;
    
    var NumberCart = 0 ;
    var NumberFavourite = 0 ;
    
    
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    // Load Dữ Liệu Ban Đầu vào Trang
    onPageLoading();
    
    // Lấy Danh Sách Sản Phẩm Cho tất cả sản phẩm
    $("#all-product-btn").on("click", function(){
      console.log("ok");
      callApiGetListProduct();
    })
    // Lấy Danh Sách Sản Phẩm Cho women
    $(".women").on("click", function(){
      console.log("ok");
      callApiGetListProductBySex("NU");
    })
    
    // Lấy Danh Sách Sản Phẩm Cho Nam
    $(".men").on("click", function(){
      console.log("ok");
      callApiGetListProductBySex("NAM");
    })
    // Lấy Danh Sách Sản Phẩm Dior
    $(".dior").on("click", function(){
      console.log("ok");
      callApiGetListProductByVendor("DIOR");
    })
    
    // Lấy Danh Sách Sản Phẩm Dior
    $(".chanel").on("click", function(){
      console.log("ok");
      callApiGetListProductByVendor("CHANEL");
    })
    
    // Lấy Danh Sách Sản Phẩm Dior
    $(".versace").on("click", function(){
      console.log("ok");
      callApiGetListProductByVendor("Versace");
    })
    
    // Lấy Danh Sách Sản Phẩm Theo Hang Nuoc Hoa
    $(".hangNuocHoa").on("click", function(){
      callApiGetListProductByVendor($(this).text());
    })
    
    // Lấy Danh Sách Sản Phẩm Theo Giá Nuoc Hoa
    $(".MucGia").on("click", function(){
      var buttonValue = $(this).text();
     console.log(buttonValue);
      getApiProductByPrice(buttonValue);
      $(".offcanvas-menu-wrapper").removeClass("active");
      $(".offcanvas-menu-overlay").removeClass("active");
    })
    


    $("#inp-search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      if(value != ""){
        callApiSearchName(value);
      }else{
        callApiGetListProduct();
      }
      // $("#div-card-realestate").filter(function() {
      //   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        
      // });
    });
  
      // Sự Kiện Khi Ckick Chọn Mùi Hương
    $(".MuiHuong").on("click" , function(){
      var valuaCheckbox = $(this).text()
      console.log(valuaCheckbox)
      callApiGetListProductBySacle(valuaCheckbox);
    })
      // Sự Kiện Khi Ckick Chọn Dung tích
    $(".dungLuong").on("click" , function(){
      var valuaCheckbox = $(this).text()
      var vDungLuong = valuaCheckbox.split('ml').join('');
      callApiGetListProductByDungLuong(vDungLuong);
    })
      // Sự Kiện Khi Ckick Hiện Thông Tin Sản Phẩm Ra Modal
    $(document).on("click" ,".product-details-a", function(){
      getProductInfo($(this).attr("data"));
      $("#ProductDetails-Modal").modal("show");
    })
    
      // Sự Kiện Khi Ckick Cho Sản Phẩm Vào giỏ Hàng
    $(document).on("click" ,".product-Buy-a", function(){
      console.log($(this).attr("data"));
      NumberCart += 1 ;
      $(".add-number-cart").text(NumberCart)
      getProductInfoAddToCart($(this).attr("data"));
    })
      // Sự Kiện Khi Ckick Thêm Sản Phẩm Yêu Thích
    $(document).on("click" ,".product-Like-a", function(e){
      var vProductId = $(this).attr("data");
      e.preventDefault();
      // var Img = $(this).closest('.product__item__pic').children("img").attr("src");
      callApiAddFavouriteProdut(vProductId);
      NumberFavourite += 1 ;
      $(".add-number-favourite").text(NumberFavourite)
    })

      // Sự Kiện Khi Ckick Chọn Dung tích
    $(document).on("click" ,".btn-buy-price", function(){

    })
  
    // Tăng Số Lượng Mua
    $(document).on("click", ".up-quantity" , function(e){
      var vInpQuantity = $(this).closest('.mark-quantity').children("input").val();
      var vInpPrice = $(this).closest('.div-mark-conten').children(".mark-price").children("span").text();
      console.log(vInpQuantity);
      var total =  parseInt(vInpQuantity) + 1 ;
      $(this).closest('.mark-quantity').children("input").val(total);
      $(this).closest('.div-mark-conten').children(".mark-total-price").children("span").text(vInpPrice *total);
      // $(this).closest('.mark-quantity').children("img").attr("src");
    })
    // Giảm Số Lượng Mua
    $(document).on("click", ".down-quantity" , function(e){
      var vInpQuantity = $(this).closest('.mark-quantity').children("input").val();
      var vInpPrice = $(this).closest('.div-mark-conten').children(".mark-price").children("span").text();
      // $(this).closest('.div-mark-conten').children(".mark-total-price").children("span").text(vInpPrice *total);
      console.log(vInpPrice);
      var total =  parseInt(vInpQuantity) - 1 ;
      if(total < 1){
        $(this).closest('.mark-quantity').children("input").val(1);
        $(this).closest('.div-mark-conten').children(".mark-total-price").children("span").text(vInpPrice);
      }else{
        $(this).closest('.mark-quantity').children("input").val(total);
        $(this).closest('.div-mark-conten').children(".mark-total-price").children("span").text(vInpPrice *total);
      }
    })

    
    //  Checkbox all
    $(document).on("change", "#checkboxmark-all" , function(e){
      var vAllCheckBox = $(this).is(":checked");
      if(vAllCheckBox){
        $(".cart-checkbox").prop("checked" , true)
      }else{
        $(".cart-checkbox").prop("checked" , false)
      }
    })
    
    // Xóa Sản Phẩm Trong Giỏ Hàng
    $(document).on("click", ".mark-delete-product" , function(e){
      e.preventDefault();
      var ProductId = $(this).attr("dataId");
      console.log(ProductId)
    })
    //  Carousel
    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:true,
      autoplay:true,
      autoplayTimeout:2500,
      autoplayHoverPause:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
  })

  $(".btn-login").on("click",function(){
    console.log("ok")
  })
    
      
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    
    // Load trang
    function onPageLoading(){
        // Lấy Danh Sách Sản Phẩm
        callApiGetListProduct();
    
    }
    
    // Get Api Product By Price
    function getApiProductByPrice(data){
      "use strict"
      var Muc0 = [0 , 1000];
      var Muc1 = [1000 , 2000];
      var Muc2 = [2000 , 3000];
      var Muc3 = [3000 , 4000];
      var Muc4 = [4000 , 5000];
      var Muc5 = [5000 , 6000];
      var Muc6 = [6000 , 10000];
      var Muc7 = [10000 , 100000];
      if(data == "Dưới 1 Triệu"){
        callApiGetListProductByPrice(Muc0);
      }else if(data == "1 -> 2 Triệu"){
        callApiGetListProductByPrice(Muc1);
      }
      else if(data == "2 -> 3 Triệu"){
        callApiGetListProductByPrice(Muc2);
      }
      else if(data == "3 -> 4 Triệu"){
        callApiGetListProductByPrice(Muc3);
      }
      else if(data == "4 -> 5 Triệu"){
        callApiGetListProductByPrice(Muc4);
      }
      else if(data == "5 -> 6 Triệu"){
        callApiGetListProductByPrice(Muc5);
      }
      else if(data == "6 -> 10 Triệu"){
        callApiGetListProductByPrice(Muc6);
      }
      else if(data == "Trên 10 triệu"){
        callApiGetListProductByPrice(Muc7);
      } 
    }
    
    
    
    
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    // Lấy Danh Sách Sản Phẩm Theo Giá Tiền
    function callApiGetListProductByPrice(Data){
      $.ajax({
        url: LocalURL + "Products/Price/" + Data[0] + "/" + Data[1],
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          callApiGetListProduct();
        }
      });
    }
    // Lấy Danh Sách Sản Phẩm Theo Dung Luong
    function callApiGetListProductByDungLuong(Data){
      $.ajax({
        url: LocalURL + "Products/Dungtich/" + Data ,
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          callApiGetListProduct();
        }
      });
    }
    // Lấy Danh Sách Sản Phẩm Theo Mùi Hương
    function callApiGetListProductBySacle(Data){
      $.ajax({
        url: LocalURL + "Products/scale/" + Data ,
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          callApiGetListProduct();
        }
      });
    }
    // Lấy Danh Sách Sản Phẩm Theo Tên Sản Phẩm
    function callApiSearchName(Data){
      $.ajax({
        url: LocalURL + "Products/" + Data + "/0",
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          callApiGetListProduct();
        }
      });
    }
    // Lấy Danh Sách Sản Phẩm Theo Giới Tính
    function callApiGetListProductBySex(DataClick){
      $.ajax({
        url: LocalURL + "Products/Sex/" + DataClick,
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          //console.log(paramError);
        }
      });
    }
    // Lấy Danh Sách Sản Phẩm Theo Hãng
    function callApiGetListProductByVendor(DataClick){
      $.ajax({
        url: LocalURL + "Products/vendor/" + DataClick,
        method: "GET",
        async: false,
        success: function (paramResponseObj) {
             console.log(paramResponseObj);
             paginationAddForm(paramResponseObj);
        },
        error: function (paramError) {
          //console.log(paramError);
        }
      });
    }
    
    // Lấy danh sách sản phẩm
    function callApiGetListProduct(){
        $.ajax({
            url: LocalURL + "Products",
            method: "GET",
            async: false,
            success: function (paramResponseObj) {
                 paginationAddForm(paramResponseObj);
            },
            error: function (paramError) {
              //console.log(paramError);
            }
          });
    }
    
    // Phân trang Cho Dữ Liệu Tải Về
    function paginationAddForm(dataAddForm){
        $(function() {
          (function(name) {
            var container = $('#pagination-' + name);
            var sources = dataAddForm;
        
            var options = {
              dataSource: sources,
              pageSize: 12,
              callback: function (response, pagination) {
                // window.console && console.log(response, pagination);
                var  vCard = "";
                $.each(response, function (index, item) {
                  vCard  += `
                 
                  <div class="product__item">
                      <div class="product__item__pic">
                      <img src="`+ item.photo +`" alt="">
                          <ul class="product__hover">
                            <li><a class="product-details-a" data="`+item.id+`"><span class="fa-solid fa-circle-info"></span></a></li>
                            <li><a class="product-Like-a" data="`+item.id+`"><span class="icon_heart_alt"></span></a></li>
                            <li><a class="product-Buy-a" data="`+item.id+`"><span class="icon_bag_alt"></span></a></li>
                          </ul>
                      </div>
                      <div class="product__item__text">
                        <div class="metmoi">
                        <h6><a href="#">`+ item.productName +`</a></h6>
                        </div>
                          <label>Dung Tích : <span>`+ item.dungTich +` ml</span></label><br>
                          <label>Nước Hoa : <span>`+ item.gioiTinh +`</span></label>
                          <div class="product__price">Giá Bán: <span>`+ item.buyPrice+`k VNĐ</span></div>
                          <div class="rating">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                          </div>
                          <div> <button class="btn btn-danger btn-buy-price" data="`+item.id+`">Mua Ngay</button></div>
                      </div>
                  </div>
             
                    `;
    
                });
       
                container.prev().html(vCard);
              }
            };
        
            $.pagination(container, options);
        
            container.addHook('beforeInit', function () {
              window.console && console.log('beforeInit...');
            });
            container.pagination(options);
        
            container.addHook('beforePageOnClick', function () {
              window.console && console.log('beforePageOnClick...');
              //return false
            });
          })('demo1');
        })
      }

  // get Thông Tin 1 Sản Phẩm 
  function getProductInfo(dataId){
    "use strict"
    $.ajax({
      url: LocalURL + "Products/info/" + dataId,
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        addThongTinVaoModalSanPham(paramResponseObj);
      },
      error: function (paramError) {
        //console.log(paramError);
      }
    });
  }
  // get Thông Tin 1 Sản Phẩm 
  function getProductInfoAddToCart(dataId){
    "use strict"
    $.ajax({
      url: LocalURL + "Products/info/" + dataId,
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        console.log(paramResponseObj)
        addDataToCartTable(paramResponseObj);
      },
      error: function (paramError) {
      }
    });
  }
      
  // get Thông Tin 1 Sản Phẩm 
  function callApiAddFavouriteProdut(dataId){
    "use strict"
    $.ajax({
      url: LocalURL + "Products/info/" + dataId,
      method: "GET",
      async: false,
      success: function (paramResponseObj) {
        console.log(paramResponseObj)
        addDataToFavouriteTable(paramResponseObj);
      },
      error: function (paramError) {
      }
    });
  }
      

  // add thông tin sản phẩm vào modal
  function addThongTinVaoModalSanPham(dataResponse){
    "use strict"
    console.log(dataResponse);
    $("#modal-img-product").attr("src",dataResponse.photo);
    $("#h3-product-name").text(dataResponse.productName);
    $("#span-mui-huong").text(dataResponse.productScale);
    $("#span-hang-nuochoa").text(dataResponse.productVendor);
    $("#span-dungluong").text(dataResponse.dungTich);
    $("#span-gioitinh").text(dataResponse.gioiTinh);
    $("#span-description").text(dataResponse.productDescripttion);
    $("#span-product-price").text(dataResponse.buyPrice);
  }

  //  gán Data vào Giỏ Hàng
  function addDataToCartTable(data){
    console.log(data);
    "use strict"
    var CartObj = `
    <div class="div-mark-conten">
      <div class="size__list">
          <label for="checkboxmark`+ data.id +`">
              <input type="checkbox" class="cart-checkbox" id="checkboxmark`+ data.id +`">
              <span class="checkmark"></span>
          </label>
      </div>
      <div class="mark-product">
          <img src="`+ data.photo +`" alt="ảnh sản phẩm">
          <div class="mark-product-name"> `+ data.productName +`</div>
      </div>
      <div class="mark-price"> <span>`+ data.buyPrice+`</span>K vn₫</div>
      <div class="mark-quantity">
          <button class="down-quantity"><i class="fa-solid fa-minus"></i></button>
          <input type="text" name="inp-mark-quantity" value="1">
          <button class="up-quantity"><i class="fa-solid fa-plus"></i></button>
      </div>
      <div class="mark-total-price"><span>`+ data.buyPrice+`</span>K vn₫</div>
      <div class="mark-delete-product" dataId="`+ data.id +`"><i class="fa-solid fa-trash"></i></div>
    </div>` ;

    var cartBody = $("#bodycartDiv");
    cartBody.append(CartObj)

  }
  //  gán Data vào Giỏ Hàng
  function addDataToFavouriteTable(data){
    console.log(data);
    "use strict"
    var FavouriteObj = `
    <div class="div-heart-alt">
      <div class="heart-product-icon" data="`+ data.id +`"><i class="fa-solid fa-heart"></i></div>
      <div class="heart-product-name">`+ data.productName +`</div>
      <div class="heart-product-img"><img src="`+ data.photo +`" alt="ảnh Sản Phẩm Yêu thích"></div>
    </div>` 
    ;

    var FavouriteBody = $(".div-heart-alt1");
    FavouriteBody.append(FavouriteObj)

  }



})